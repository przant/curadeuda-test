# **CURADEUDA BACKEND DEVELOPER JR.**

Proyecto de prueba para aplicar a la vacante de _backend developer jr._, el 
desarrollo de la prueba se realizará en una plataforma _Linux_, especificamente 
en _Ubuntu 20.04_ por medio de _Windows Subsytem for Linux (WSL)_ en una latop 
con sistema operativo principal _Windows 10_.  

El software a utilizar para el desarrollo de los componentes de la prueba son 
los siguientes:  

1. **SO Ubuntu 20.04**
2. **Python 3.8.5**
3. **Flask 1.1.2**
4. **MySQL Connector 8.0.22 Platform Independent**
5. **RDBMS MariaDB 10.3 (Versión incorporada en Ubuntu 20.04)**
6. **IDE Visual Studio Code 1.55**

Donde el software a tener instalado en el sistema para poder ejecutar la prueba 
de forma adecuada, es el software listado en los puntos _1-5_

## INSTRUCCIONES DE INSTALACIÓN DE SOFTWARE

Las instrucciones de instalación siguientes se realizaron en 
_SO Ubuntu 20.04, Focal Fossa_, por lo cual, si se esta utilizando un sistema 
operativo dieferente consultar guis de instalación para el sistema en uso.

1. Actualzar el listado de paquetes del sistema mediante  
~~~bash
    sudo apt-get update && sudo apt-get upgrade
~~~

2. Instalar _RDBMS MariaDB 10.3_ : 
    - Instalar _MariaDB 10.3_ y sus dependencias mediante:
    ~~~bash
    sudo apt-get install mariadb-server-10.3
    ~~~
    - _Post_ instalación, ejecutar comando _mysql_secure_installation_:
    ~~~bash
    mysql_secure_installation
    ~~~
    - Pedira ingresar la contraseña _root_, en la pregunta donde indica modificar
    la contraseña de _root_ inidcar no (_n_) y en las preguntas restantes
    indicar si (_y_)
    - Posterior a ello se puede cear de forma opcional un usuario para utilizar
    durante el desarrollo de la prueba o se puede utilizar el usuario root.  
    NOTA: El o los usuarios mencionados, son usuarios de MariaDB no del Sistema.


3. Descargar e instalar el connector para utilizar con el lenguaje Python, para
 ello descargar el siguiente archivo 
 [MySQL Connector](https://downloads.mysql.com/archives/get/p/29/file/mysql-connector-python-8.0.22.tar.gz), 
 del cual se esta usando la version de plataforma independiente.
  - Desempaquetar el conector por medio de la siguiente instrucción:  
  ~~~bash
  tar xzfv mysql-connector-python-xx.xx.xx.tar.gz
  ~~~
  - Cambiarse al directorio que se genera de la extracción de los archivos  
  ~~~bash
  cd mysql-connector-python-xx.xx.xx
  ~~~ 
  - Instalar **MySQL Connector** mediante la instrucción  
  ~~~bash
  sudo -H python3 setup.py install
  ~~~

4. Verificar si se tiene _Python_ instalado
  ~~~sh
    python3 -V
  ~~~
  El comando anterior debe de dar una salida similar a **Python 3.8.5**, donde
  se debe de validar que la versión sea igual o mayor a **3.8.5**.
  Si no se encuentra instalado _Python_ instalar mediante:
  ~~~sh
    sudo apt-get install python3.8
  ~~~

5. Instalar o verificar que esta instalado _pip3_.
  ~~~sh
  pip3 -V
  ~~~
  Devuelve una salida similar a **pip 20.0.2 from /usr/lib/python3/dist-packages/pip (python 3.8)**
  Si no se encuentra instalado, realizar la instalacion:
  ~~~sh
  sudo apt-get install pip3 -V
  ~~~

6. Instalar **Flask** y sus dependencias
  ~~~sh
  pip3 install Flask
  ~~~




## INSTRUCCIONES DE EJECUCIÓN

Posterior a la instalacion del software requerido, se realizan los soguientes pasos para ejecutar la verificación del desarrollo del proyecto, que incluye los puntos: 1, 2, 3, 4 y
 5 hasta el iniciso b:

1. Crear una carpeta en donde se clonara el repositorio y moverse dentro de la carpeta
  ~~~sh
    mkdir <dir_name> && cd <dir_name> 
  ~~~
2. Clonar el repositorio mediante alguno de los siguientes comandos:
  ~~~sh
  # via ssh
  git clone git@gitlab.com:apem/curadeuda-test.git
  ~~~
  ~~~sh
  # via https
  git clone https://gitlab.com/apem/curadeuda-test.git
  ~~~

3. Con el repositorio descargado moverse a la carpeta _../curadeuda-test/sql/_ 
y ejecutar la instrucción siguiente:
  ~~~sh
  mysql -u <usuario> -p < create_db_script.sql
  ~~~
  El cual cargara los datos en las diferentes tablas a utilizar

4. Posterior a la carga de los datos, salir de la carpeta _/curadeuda-test/sql/_
y moverse a la carpeta _/curadeuda-test/python/_:
  ~~~sh
    cd  ..; cd python/
  ~~~

5. Para asegurar la ejecución de la API desarrollada con Python, aginar permisos
  de ejecución al archivo **web_api.py** y **DB.py**
  ~~~sh
    chmod +x eb_api.py DB.py
  ~~~

6. Ejecutar el script **web_api.py** para probar la API
  ~~~sh
    ./web_api.py
  ~~~

7. Probar los _endpoints_, con los sigueintes casos de prueba y/o cambiando valores
  - Obtener el catalogo de estados: http://localhost:8000/estados
  - Obtener los valores de un estado http://localhost:8000/estados/Ciudad de México
  - Obtener listado de Municipios: http://localhost:8000/municipios
  - Obtener listado de Municipios por nombre: http://localhost:8000/municipios/Iztapalapa
  - Obtener listado de colonias: http://localhost:8000/colonias
  - Obtener Listado de colonias por codigo postal: http://localhost:8000/colonias/09790
  - Obtener Listado de colonias por codigo postal: http://localhost:8000/colonias/09940
  - Obtener Listado de colonias por nombre: http://localhost:8000/colonias/Chamilpa
  - Obtener Listado de colonias por nombre: http://localhost:8000/colonias/Jardines de San Lorenzo Tezonco
  - Obtener Listado de colonias por nombre: http://localhost:8000/colonias/San Lorenzo
